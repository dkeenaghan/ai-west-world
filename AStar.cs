﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dkeenaghan.westworld
{
    static class AStar
    {
        /// <summary>
        /// Uses A* to find the shortest path between two nodes
        /// </summary>
        /// <param name="start">Start node</param>
        /// <param name="goal">End node</param>
        /// <returns>The goal node containing a link to the previous node (Parent) along the shortest path</returns>
        public static IAStarNode FindShortest(IAStarNode start, IAStarNode goal)
        {
            ASOpenSet open = new ASOpenSet();
            open.Add(start);

            ASClosedSet closed = new ASClosedSet();
            IAStarNode current = open.GetLowest();
            List<IAStarNode> neighbours;

            while(current != goal)
            {
                current = open.GetLowest();
                open.Remove(current);
                closed.Add(current);

                neighbours = current.GetNeighbours();
                foreach(var neighbour in neighbours)
                {
                    double cost = current.G + current.CalculateCostTo(neighbour);

                    if(cost < neighbour.G && open.Contains(neighbour))
                        open.Remove(neighbour);
                    if(cost < neighbour.G && closed.Contains(neighbour))
                        closed.Remove(neighbour);
                    if(!open.Contains(neighbour) && !closed.Contains(neighbour))
                    {
                        neighbour.G = cost;
                        neighbour.F = cost + neighbour.H;
                        open.Add(neighbour);
                        neighbour.ParentNode = current;
                    }
                }
            }

            return current;
        }
        
        /// <summary>
        /// Walks along the linked list produced by the FindShortest A* method and returns a list representing the route, including the start node
        /// </summary>
        /// <param name="goal">Goal node given by the FindShortest method</param>
        /// <returns>A list representing the shortest path</returns>
        public static List<IAStarNode> GetForwardRoute(IAStarNode goal)
        {
            Stack<IAStarNode> route = new Stack<IAStarNode>();
            route.Push(goal);

            while(goal.ParentNode != null)
            {
                route.Push(goal.ParentNode);
                goal = goal.ParentNode;
            }

            return route.ToList();
        }
    }
}
