﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dkeenaghan.westworld.Sheriff
{
    public class Sheriff : Agent
    {        
        public StateMachine<Sheriff> StateMachine { get; set; }
        public int GoldAmount { get; set; }

        public Sheriff() : base()
        {
            names = new string[] { "Wyatt", "P.C. Plod" };
            Name = GenerateName();

            Position = Game1.Instance.WorldMap.PositionOf(LocationType.SherrifsOffice);

            StateMachine = new StateMachine<Sheriff>(this);
            StateMachine.CurrentState = new GoToOffice();
            StateMachine.GlobalState = new SheriffGlobalState();
        }

        public override void Update()
        {
            StateMachine.Update();
        }

        public override bool HandleMessage(Telegram telegram)
        {
            return StateMachine.HandleMessage(telegram);
        }

        public override void OnSense(SenseEventArgs e)
        {
            List<Agent> agents = new List<Agent>();

            foreach(var signal in e.Signals)
            {
                // Reflection is used to check if the encountered agent is an outlaw,
                // this ought to be replace with a system whereby each agent has an 'AgentType' property
                // that can be checked against, but this will do, even though reflection isn't the fastest thing in the world.
                if(signal.Emitter is Outlaw)
                {
                    if(((Outlaw)signal.Emitter).IsAlive)
                    {
                        Printer.Print(this.Name, "###  B O O M  ###");
                        StateMachine.ChangeState(new EncounterOutlaw((Outlaw)signal.Emitter));
                    }
                }
                else if(signal.Emitter is Agent)
                    Printer.Print(this.Name, "Howdy");
            }
        }
    }
}
