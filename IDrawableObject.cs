﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    /// <summary>
    /// Base for all drawable objects in the game
    /// </summary>
    public interface IDrawableObject
    {
        Vector2 Position { get; set; }

        void Update(GameTime gameTime);
        void Draw(GameTime gameTime);        
    }
}
