﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    public class MovementNode : IAStarNode
    {
        public double F { get; set; }
        public double G { get; set; }
        public double H { get; set; }
        public AStarGraph ParentGraph { get; set; }
        public IAStarNode ParentNode { get; set; }

        public Vector2 Position { get; set; }
        public LocationType LocationType { get; set; }

        public List<IAStarNode> GetNeighbours()
        {
            List<IAStarNode> neighbours = new List<IAStarNode>();
            int[] pos = new int[] { -1, 0, 0, 1, 1, 0, 0, -1 };

            for(int i = 0; i < pos.Length; i += 2)
            {
                IAStarNode n = ParentGraph.Get((int)(Position.X + pos[i]), (int)(Position.Y + pos[i + 1]));

                if(n != null)
                    neighbours.Add(n);
            }

            return neighbours;
        }

        public double CalculateCostTo(IAStarNode destination)
        {
            Vector2 destPos = ParentGraph.GetPositionOf(destination);
            float cost = Math.Abs(Position.X - destPos.X) + Math.Abs(Position.Y - destPos.Y);

            switch(LocationType)
            {
                case LocationType.Empty:
                    break;
                case LocationType.Mountain:
                    cost += 15;
                    break;
                default:
                    cost += 1;
                    break;
            }

            return cost;
        }
    }
}
