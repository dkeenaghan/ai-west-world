﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dkeenaghan.westworld
{
    class ASClosedSet
    {
        private HashSet<IAStarNode> items = new HashSet<IAStarNode>();

        public void Add(IAStarNode item)
        {
            items.Add(item);
        }

        public bool Contains(IAStarNode item)
        {
            return items.Contains(item);
        }

        public void Remove(IAStarNode item)
        {
            items.Remove(item);
        }
    }
}
