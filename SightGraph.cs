﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    public class SightGraph : AStarGraph
    {
        public SightGraph(int width, int height) : base(width, height)
        {
            
        }

        public float GetDistance(Vector2 start, Vector2 destination)
        {
            IAStarNode goalNode = AStar.FindShortest(Get(start), Get(destination));
            List<IAStarNode> route = AStar.GetForwardRoute(goalNode);
            ResetAll();

            float distance = 0;

            foreach(var item in route)
            {
                distance += GetSightPropagationDistance(Game1.Instance.WorldMap.Get(((SightNode)item).Position).LocationType); // pretty
            }

            return distance;
        }

        public static float GetSightPropagationDistance(LocationType locationType)
        {
            switch(locationType)
            {
                case LocationType.Mountain:
                    return 10;
                case LocationType.Empty:
                    return 0.5f;
                case LocationType.Cemetery:
                    return 1.6f;
                default: // buildings
                    return 1;
            }
        }
    }
}
