﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dkeenaghan.westworld
{
    class ASOpenSet
    {
        private HashSet<IAStarNode> items = new HashSet<IAStarNode>();
        private IAStarNode lowest = null;

        public void Add(IAStarNode item)
        {
            items.Add(item);

            // Check if item to be added is the lowest item, if so then replace the current lowest item
            if(lowest == null || item.F < lowest.F)
                lowest = item;
        }
        
        public bool Contains(IAStarNode item)
        {
            return items.Contains(item);
        }

        public void Remove(IAStarNode item)
        {
            items.Remove(item);

            // if the lowest has been removed then find the next lowest
            if(lowest != item || items.Count == 0)
                return;

            // Search items for new lowest item
            IAStarNode min = null;
            foreach(var entry in items)
            {
                if(min == null || entry.F < min.F)
                    min = entry;
            }

            lowest = min;
        }

        public IAStarNode GetLowest()
        {
            return lowest;
        }
    }
}
