using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    public static class AgentManager
    {
        private static Dictionary<int, Agent> listOfAgents = new Dictionary<int, Agent>();
        public static Dictionary<int, Agent> Agents { get { return listOfAgents; } }

        public static void AddAgent(Agent agent)
        {
            listOfAgents.Add(agent.ID, agent);
        }

        public static Agent GetAgent(int id)
        {
            return listOfAgents[id];
        }

        public static void RemoveAgent(int agentID)
        {
            listOfAgents.Remove(agentID);
        }

        public static List<Agent> GetAgents(Func<Agent, bool> pred)
        {
            List<Agent> agents = new List<Agent>();

            foreach(var item in listOfAgents)
            {
                if(pred(item.Value))
                    agents.Add(item.Value);
            }

            return agents;
        }

        public static void Update(GameTime gameTime)
        {
            foreach(var agent in Agents)
            {
                agent.Value.Update();
            }
        }
    }
}
