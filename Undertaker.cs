﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld.Undertaker
{
    public class Undertaker : Agent
    {
        public StateMachine<Undertaker> StateMachine { get; set; }
        public Queue<Vector2> CorpseLocations { get; set; }

        public Undertaker()
        {
            names = new string[] { "Robert", "Johnny", "Rudolf" };
            Name = GenerateName();

            Position = Game1.Instance.WorldMap.PositionOf(LocationType.Undertakers);
            StateMachine = new StateMachine<Undertaker>(this);
            StateMachine.CurrentState = new WaitingForDeaths();
            StateMachine.GlobalState = new UndertakerGlobalState();

            CorpseLocations = new Queue<Vector2>();
        }

        public override void Update()
        {
            StateMachine.Update();
        }

        public override bool HandleMessage(Telegram telegram)
        {
            return StateMachine.HandleMessage(telegram);
        }
    }
}
