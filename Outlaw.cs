﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dkeenaghan.westworld
{
    public class Outlaw : Agent
    {
        public StateMachine<Outlaw> StateMachine { get; set; }
        public int GoldAmount { get; set; }

        public Outlaw() : base()
        {
            names = new string[] { "Jesse", "Tim" };
            Name = GenerateName();

            Position = Game1.Instance.WorldMap.PositionOf(LocationType.OutlawCamp);

            StateMachine = new StateMachine<Outlaw>(this);
            StateMachine.CurrentState = new VisitOutlawCamp();
            StateMachine.GlobalState = new OutlawGlobalState();

            GoldAmount = 0;
        }

        public int TakeGold()
        {
            int gold = GoldAmount;
            GoldAmount = 0;
            return gold;
        }

        public override void Update()
        {
            StateMachine.Update();
        }

        public override bool HandleMessage(Telegram telegram)
        {
            return StateMachine.HandleMessage(telegram);
        }
    }
}
