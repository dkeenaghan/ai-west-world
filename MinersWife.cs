using System;
using System.Collections.Generic;
using System.Text;

namespace dkeenaghan.westworld
{
    // This class implements a MinersWife agent; the agent creates and maintains its own 
    // StateMachine that it invokes whenever the game asks it to update (triggered
    // by XNA's Update() method)
    public class MinersWife : Agent
    {
        public StateMachine<MinersWife> StateMachine { get; set; }
        // This is used to keep track of which other agent is our husband
        public int HusbandID { get; set; }
        public Boolean Cooking { get; set; }

        // The constructor invokes the base class constructor, which then creates 
        // an id for the new agent object and then creates and initalises the agent's
        // StateMachine
        public MinersWife() : base()
        {
            names = new string[] { "Elsa" };
            Name = GenerateName();
            Position = Game1.Instance.WorldMap.PositionOf(LocationType.Shack);

            StateMachine = new StateMachine<MinersWife>(this);
            StateMachine.CurrentState = new DoHouseWork();
            StateMachine.GlobalState = new WifesGlobalState();
        }

        // This method is invoked by the Game object as a result of XNA updates 
        public override void Update()
        {
            StateMachine.Update();
        }

        // This method is invoked when the agent receives a message
        public override bool HandleMessage(Telegram telegram)
        {
            return StateMachine.HandleMessage(telegram);    
        }
    }
}
