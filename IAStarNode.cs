﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{    
    public interface IAStarNode
    {
        double F { get; set; }
        double G { get; set; }
        double H { get; set; }
        AStarGraph ParentGraph { get; set; }
        IAStarNode ParentNode { get; set; }

        List<IAStarNode> GetNeighbours();
        double CalculateCostTo(IAStarNode destination);
    }
}
