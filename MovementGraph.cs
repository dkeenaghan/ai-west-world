﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    public class MovementGraph : AStarGraph
    {
        public MovementGraph(int width, int height) : base(width, height)
        {
            
        }

        public List<IAStarNode> GetRoute(Vector2 start, Vector2 destination)
        {
            IAStarNode goalNode = AStar.FindShortest(Get(start), Get(destination));
            List<IAStarNode> route = AStar.GetForwardRoute(goalNode);

            ResetAll();
            return route;
        }
    }
}
