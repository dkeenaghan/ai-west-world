using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace dkeenaghan.westworld
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    class Game1 : Microsoft.Xna.Framework.Game
    {
        public static Game1 Instance { get { return _instance; } }
        private static readonly Game1 _instance = new Game1();

        public GraphicsDeviceManager Graphics;
        public SpriteBatch SpriteBatch;
        public WorldMap WorldMap;
        public SenseManager SenseManager = new SenseManager();

        private SpriteFont _spriteFont;       
        private KeyboardState _newState;
        private KeyboardState _oldState;

        private Game1()
        {
            Graphics = new GraphicsDeviceManager(this);
            Graphics.PreferredBackBufferHeight = 960;
            Graphics.PreferredBackBufferWidth = 960;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            WorldMap = new WorldMap(15, 15);

            Miner Bob = new Miner();
            MinersWife Elsa = new MinersWife();
            Sheriff.Sheriff sheriff = new Sheriff.Sheriff();

            Bob.WifeID = Elsa.ID;
            Elsa.HusbandID = Bob.ID;

            AgentManager.AddAgent(Bob);
            AgentManager.AddAgent(Elsa);
            AgentManager.AddAgent(new Outlaw());
            AgentManager.AddAgent(sheriff);
            AgentManager.AddAgent(new Undertaker.Undertaker());

            // Registers the sheriff as a sensor - for this there's only the one sensor, but each agent could also be a sensor
            SenseManager.RegisterSensor(sheriff);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            _spriteFont = Content.Load<SpriteFont>("Arial");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        private double timeAccumulator = 0.45;
        protected override void Update(GameTime gameTime)
        {
            _newState = Keyboard.GetState();

            if(_newState.IsKeyDown(Keys.Escape))
                this.Exit();

            if(_newState.IsKeyDown(Keys.T) && !_oldState.IsKeyDown(Keys.T))
                WorldMap.ShowTrails = !WorldMap.ShowTrails;

            timeAccumulator += gameTime.ElapsedGameTime.TotalSeconds;
            if(timeAccumulator > 0.5)
            {
                timeAccumulator = 0;
                Message.gameTime = gameTime;

                AgentManager.Update(gameTime);
                WorldMap.Update(gameTime);
                SenseManager.Update(gameTime);

                Message.SendDelayedMessages();
                base.Update(gameTime);
            }
            
            _oldState = _newState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            Graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

            WorldMap.Draw(gameTime);

            if(Keyboard.GetState().IsKeyDown(Keys.P))
                Printer.Draw(SpriteBatch, _spriteFont);

            base.Draw(gameTime);
        }
    }
}
