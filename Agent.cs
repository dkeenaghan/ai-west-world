using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace dkeenaghan.westworld
{
    abstract public class Agent : IDrawableObject, ISensor, IEmitter
    {
        public static Random Random = new Random();
        private static int agents = 0;
        public Vector2 Position { get; set; }
        public LocationType LocationType { get { return Game1.Instance.WorldMap.Get(Position).LocationType; } }
        public SightSignal _sightSignal;
        public bool IsAlive { get; set; }

        // Every agent has a numerical id that is set when it is created
        public int ID { get; set; }
        public string Name { get; set; }
        // Default names, one is chosen randomly
        protected string[] names = new string[] { "Jack", "Jill", "Merry", "Pipin" };
        protected Texture2D _texture;

        public Agent()
        {
            ID = agents++;
            Name = GenerateName();
            IsAlive = true;
            _texture = Game1.Instance.Content.Load<Texture2D>(this.GetType().Name);

            _sightSignal = new SightSignal(Position, this);
            Game1.Instance.SenseManager.AddSignal(_sightSignal);
        }

        /// <summary>
        /// Creates a random name for the agent
        /// </summary>
        /// <returns>Random name for the agent</returns>
        public string GenerateName()
        {
            return names[Random.Next(names.Length)];
        }

        // Any agent must implement these methods
        abstract public void Update();
        abstract public bool HandleMessage(Telegram telegram);
        
        public void Update(GameTime gameTime)
        {
            _sightSignal.Position = Position;
        }

        public void Draw(GameTime gameTime)
        {
            Vector2 pos = new Vector2(Position.X * _texture.Width, Position.Y * _texture.Height);

            Game1.Instance.SpriteBatch.Draw(_texture, pos, Color.White);
        }
        
        public virtual bool DetectsSense(Signal signal)
        {
            return signal.Type == SenseType.Sight;
        }

        public virtual float DistanceTo(Signal signal)
        {
            return Game1.Instance.WorldMap.SightGraph.GetDistance(Position, signal.Position);
        }

        public virtual float DetectRange(Signal signal)
        {
            return Game1.Instance.WorldMap.IsNightTime ? 1.2f : 2.0f;
        }

        public virtual void OnSense(SenseEventArgs e)
        {
            // overridden by sub-classes
        }
    }
}
