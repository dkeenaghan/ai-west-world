using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    public class TravellingMinerState : State<Miner>
    {
        Vector2 _destination;
        Stack<WorldMapNode> _route;
        State<Miner> _nextState;

        public TravellingMinerState(State<Miner> nextState)
        {
            _nextState = nextState;
            _destination = Game1.Instance.WorldMap.PositionOf(nextState.GetInitialLocation());
        }

        public override void Enter(Miner agent)
        {
            _route = Game1.Instance.WorldMap.GetRoute(agent.Position, _destination);
        }

        public override void Execute(Miner agent)
        {
            if(_route.Count == 0)
            {
                agent.StateMachine.ChangeState(_nextState);
                return;
            }

            WorldMapNode current = _route.Pop();
            agent.Position = current.Position;
            current.HighlightIndex = agent.ID;
            current.IsHighlighted = true;
        }

        public override void Exit(Miner agent)
        {
            Game1.Instance.WorldMap.UnhighlightAll(agent.ID);
        }

        public override bool OnMesssage(Miner agent, Telegram telegram)
        {
            return false;
        }

        protected virtual WorldMapNode NextLocation()
        {
            return null;
        }
    }

    // This class implements the state in which the Miner agent mines for gold
    public class EnterMineAndDigForNugget : State<Miner>
    {
        public override LocationType GetInitialLocation()
        {
            return LocationType.GoldMine;
        }

        public override void Enter(Miner miner)
        {
            Printer.Print(miner.Name, "Walkin' to the goldmine");
        }

        public override void Execute(Miner miner)
        {
            miner.GoldCarrying += 1;
            miner.HowFatigued += 1;
            Printer.Print(miner.Name, "Pickin' up a nugget");

            if (miner.PocketsFull())
            {
                miner.StateMachine.ChangeState(new TravellingMinerState(new VisitBankAndDepositGold()));
            }
            else if (miner.Thirsty())
            {
                miner.StateMachine.ChangeState(new TravellingMinerState(new QuenchThirst()));
            }
        }

        public override void Exit(Miner miner)
        {
            Printer.Print(miner.Name, "Ah'm leaving the gold mine with mah pockets full o' sweet gold");
        }

        public override bool OnMesssage(Miner agent, Telegram telegram)
        {
            return false;    
        }
    }

    // In this state, the miner goes to the bank and deposits gold
    public class VisitBankAndDepositGold : State<Miner>
    {
        public override LocationType GetInitialLocation()
        {
            return LocationType.Bank;
        }

        public override void Enter(Miner miner)
        {
            Printer.Print(miner.Name, "Goin' to the bank. Yes siree");
        }

        public override void Execute(Miner miner)
        {
            miner.MoneyInBank += miner.GoldCarrying;
            miner.GoldCarrying = 0;
            Printer.Print(miner.Name, "Depositing gold. Total savings now: " + miner.MoneyInBank);

            if (miner.Rich())
            {
                Printer.Print(miner.Name, "WooHoo! Rich enough for now. Back home to mah li'lle lady");
                miner.StateMachine.ChangeState(new TravellingMinerState(new GoHomeAndSleepTillRested()));
            }
            else
            {
                miner.StateMachine.ChangeState(new TravellingMinerState(new EnterMineAndDigForNugget()));
            }
        }

        public override void Exit(Miner miner)
        {
            Printer.Print(miner.Name, "Leavin' the Bank");
        }

        public override bool OnMesssage(Miner agent, Telegram telegram)
        {
            return false;
        }
    }

    // In this state, the miner goes home and sleeps
    public class GoHomeAndSleepTillRested : State<Miner>
    {
        public override LocationType GetInitialLocation()
        {
            return LocationType.Shack;
        }

        public override void Enter(Miner miner)
        {
            Printer.Print(miner.Name, "Walkin' Home");
            Message.DispatchMessage(0, miner.ID, miner.WifeID, MessageType.HiHoneyImHome);
        }

        public override void Execute(Miner miner)
        {
            if (miner.HowFatigued < miner.TirednessThreshold)
            {
                Printer.Print(miner.Name, "All mah fatigue has drained away. Time to find more gold!");
                miner.StateMachine.ChangeState(new TravellingMinerState(new EnterMineAndDigForNugget()));
            }
            else
            {
                miner.HowFatigued--;
                Printer.Print(miner.Name, "ZZZZZ....");
            }
        }

        public override void Exit(Miner miner)
        {

        }

        public override bool OnMesssage(Miner miner, Telegram telegram)
        {
            switch (telegram.messageType)
            {
                case MessageType.HiHoneyImHome:
                    return false;
                case MessageType.StewsReady:
                    Printer.PrintMessageData("Message handled by " + miner.ID + " at time ");
                    Printer.Print(miner.Name, "Okay Hun, ahm a comin'!");
                    miner.StateMachine.ChangeState(new TravellingMinerState(new EatStew()));
                    return true; 
                default:
                    return false;
            }
        }
    }

    // In this state, the miner goes to the saloon to drink
    public class QuenchThirst : State<Miner>
    {
        public override LocationType GetInitialLocation()
        {
            return LocationType.Saloon;
        }

        public override void Enter(Miner miner)
        {
            Printer.Print(miner.Name, "Boy, ah sure is thusty! Walking to the saloon");
        }

        public override void Execute(Miner miner)
        {
            // Buying whiskey costs 2 gold but quenches thirst altogether
            miner.HowThirsty = 0;
            miner.MoneyInBank -= 2;
            Printer.Print(miner.Name, "That's mighty fine sippin' liquer");
            miner.StateMachine.ChangeState(new TravellingMinerState(new EnterMineAndDigForNugget()));
        }

        public override void Exit(Miner miner)
        {
            Printer.Print(miner.Name, "Leaving the saloon, feelin' good");
        }

        public override bool OnMesssage(Miner agent, Telegram telegram)
        {
            return false;
        }
    }

    // In this state, the miner eats the food that Elsa has prepared
    public class EatStew : State<Miner>
    {
        public override LocationType GetInitialLocation()
        {
            return LocationType.Shack;
        }

        public override void Enter(Miner miner)
        {
            Printer.Print(miner.Name, "Smells Reaaal goood Elsa!");
        }

        public override void Execute(Miner miner)
        {
            Printer.Print(miner.Name, "Tastes real good too!");
            miner.StateMachine.ChangeState(new TravellingMinerState(new EnterMineAndDigForNugget()));
        }

        public override void Exit(Miner miner)
        {
            Printer.Print(miner.Name, "Thankya li'lle lady. Ah better get back to da mines!");
        }

        public override bool OnMesssage(Miner agent, Telegram telegram)
        {
            return false;
        }
    }

    // If the agent has a global state, then it is executed every Update() cycle
    public class MinerGlobalState : State<Miner>
    {
        public override void Enter(Miner miner)
        {

        }

        public override void Execute(Miner miner)
        {
            miner.HowThirsty += 1;
        }
        
        public override void Exit(Miner miner)
        {

        }

        public override bool OnMesssage(Miner agent, Telegram telegram)
        {
            return false;
        }
    }
}
