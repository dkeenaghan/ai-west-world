﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    public class WorldMap : IDrawableObject
    {
        public static bool ShowTrails = true;

        private WorldMapNode[,] _nodes;
        private int _width;
        private int _height;
        private float _timeOfDay;

        public MovementGraph MovementGraph { get; set; }
        public SightGraph SightGraph { get; set; }
                
        public Vector2 Position { get; set; }
        public int Width { get { return _width; } set { _width = value; } }
        public int Height { get { return _height; } set { _height = value; } }
        public bool IsNightTime { get { return (_timeOfDay > 20 || _timeOfDay < 6); } set { _timeOfDay = 21; } }
        public bool IsTwilight { get { return (_timeOfDay > 19 || _timeOfDay < 7) && !IsNightTime; } set { _timeOfDay = 20; } }

        public WorldMap(int width, int height)
        {            
            _width = width;
            _height = height;
            _timeOfDay = 8;

            // Generate new map configurations until a valid one is gererated (ie doesn't contain duplicate (non- mountain/plain) location types)
            while(!GenerateMap());

            CreateGraphs();
        }

        private void CreateGraphs()
        {
            MovementGraph = new MovementGraph(_width, _height);
            SightGraph = new SightGraph(_width, _height);

            for(int row = 0; row < _height; row++)
            {
                for(int col = 0; col < _width; col++)
                {
                    MovementGraph.Set(col, row, new MovementNode() { 
                        ParentGraph = MovementGraph, 
                        Position = _nodes[col, row].Position, 
                        LocationType = _nodes[col, row].LocationType 
                    });

                    SightGraph.Set(col, row, new SightNode() { 
                        ParentGraph = SightGraph,
                        Position = _nodes[col, row].Position,
                        LocationType = _nodes[col, row].LocationType
                    });
                }
            }
        }

        public Stack<WorldMapNode> GetRoute(Vector2 start, Vector2 destination)
        {
            List<IAStarNode> route = MovementGraph.GetRoute(start, destination);
            Stack<WorldMapNode> r = new Stack<WorldMapNode>();

            for(int i = route.Count - 1; i >= 0; i--)
            {
                r.Push(Get(((MovementNode)route[i]).Position));
            }

            return r;
        }
        
        public WorldMapNode Get(Vector2 position)
        {
            return Get((int)position.X, (int)position.Y);
        }

        public WorldMapNode Get(int col, int row)
        {
            if(row < 0 || col < 0 || row >= _nodes.GetLength(1) || col >= _nodes.GetLength(0))
                return null;

            return _nodes[col, row];
        }

        private bool GenerateMap()
        {
            Random r = new Random();
            _nodes = new WorldMapNode[_height, _width];
            LocationType[] locations = (LocationType[])Enum.GetValues(typeof(LocationType));
            int[] positions = new int[locations.Length];

            for(int i = 0; i < positions.Length; i++)
                positions[i] = r.Next(_width * _height);

            for(int i = 0; i < positions.Length; i++)
            {
                for(int j = 0; j < positions.Length; j++)
                {
                    if(i != j && positions[i] == positions[j])
                        return false;
                }
            }
                        
            int index;
            for(int row = 0; row < _height; row++)
            {
                for(int col = 0; col < _width; col++)
                {
                    if(positions.Contains(row * _height + col))
                    {
                        for(index = 0; index < positions.Length; index++)
                        {
                            if(positions[index] == row * _height + col)
                                break;                            
                        }
                        _nodes[col, row] = new WorldMapNode(col, row, locations[index], this);
                    }
                    else if(r.Next(5) == 0)
                        _nodes[col, row] = new WorldMapNode(col, row, LocationType.Mountain, this);
                    else
                        _nodes[col, row] = new WorldMapNode(col, row, LocationType.Empty, this);
                }
            }

            return true;
        }

        public Vector2 PositionOf(LocationType location)
        {
            for(int row = 0; row < _height; row++)
            {
                for(int col = 0; col < _width; col++)
                {
                    if(location == _nodes[col, row].LocationType)
                        return new Vector2(col, row);
                }
            }

            return new Vector2(-1);
        }

        public WorldMapNode NodeOf(LocationType location)
        {
            return Get(PositionOf(location));
        }

        public void Update(GameTime gameTime)
        {
            _timeOfDay = (_timeOfDay + 0.5f) % 24;

            foreach(WorldMapNode node in _nodes)
                node.Update(gameTime);
            
            foreach(var agent in AgentManager.Agents)
                agent.Value.Update(gameTime);
        }

        public void UnhighlightAll(int index)
        {
            foreach(var node in _nodes)
            {
                if(node.HighlightIndex == index)
                    node.IsHighlighted = false;
            }
        }
        
        public void Draw(GameTime gameTime)
        {
            Game1.Instance.SpriteBatch.Begin();

            foreach(WorldMapNode node in _nodes)
                node.Draw(gameTime);
            
            foreach(var agent in AgentManager.Agents)
                agent.Value.Draw(gameTime);

            Game1.Instance.SpriteBatch.End();
        }
    }
}
