﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld.Sheriff
{
    public class TravellingSheriffState : State<Sheriff>
    {
        Vector2 _destination;
        Stack<WorldMapNode> _route;
        State<Sheriff> _nextState;

        public TravellingSheriffState(State<Sheriff> nextState)
        {
            _nextState = nextState;
            _destination = Game1.Instance.WorldMap.PositionOf(nextState.GetInitialLocation());
        }

        public override void Enter(Sheriff agent)
        {
            _route = Game1.Instance.WorldMap.GetRoute(agent.Position, _destination);
        }

        public override void Execute(Sheriff agent)
        {
            if(_route.Count == 0)
            {
                agent.StateMachine.ChangeState(_nextState);
                return;
            }

            WorldMapNode current = _route.Pop();
            agent.Position = current.Position;
            current.HighlightIndex = agent.ID;
            current.IsHighlighted = true;
        }

        public override void Exit(Sheriff agent)
        {
            // unhighlight any tiles that have been highlighted along this agent's path
            Game1.Instance.WorldMap.UnhighlightAll(agent.ID);
        }

        public override bool OnMesssage(Sheriff agent, Telegram telegram)
        {
            return false;
        }

        protected virtual WorldMapNode NextLocation()
        {
            return null;
        }
    }

    public class GoToOffice : State<Sheriff>
    {
        public override LocationType GetInitialLocation()
        {
            return LocationType.SherrifsOffice;
        }

        public override void Enter(Sheriff agent)
        {
            Printer.Print(agent.Name, "Office time");
        }

        public override void Execute(Sheriff agent)
        {
            agent.StateMachine.ChangeState(new Patrol(agent));
        }

        public override void Exit(Sheriff agent)
        {
            Printer.Print(agent.Name, "Leaving the office");
        }

        public override bool OnMesssage(Sheriff agent, Telegram telegram)
        {
            return false;
        }
    }

    public class Patrol : State<Sheriff>
    {
        private LocationType _patrolLocation;

        public override LocationType GetInitialLocation()
        {
            return _patrolLocation;
        }

        public Patrol(Agent agent)
        {
            _patrolLocation = PickRandomLocation(agent.LocationType);
        }

        public override void Enter(Sheriff agent)
        {
            Printer.Print(agent.Name, "Patrol time!");
        }

        public override void Execute(Sheriff agent)
        {            
            //List<Agent> agents = AgentManager.Agents.Where(a => a.Location == agent.Location).ToList();
            //agents.Remove(agent);

            agent.StateMachine.ChangeState(new TravellingSheriffState(new Patrol(agent)));
        }

        public override void Exit(Sheriff agent)
        {

        }

        public override bool OnMesssage(Sheriff agent, Telegram telegram)
        {
            return false;
        }

        private LocationType PickRandomLocation(LocationType current)
        {
            LocationType[] locations = (LocationType[])Enum.GetValues(typeof(LocationType));
            int rand;

            do { rand = Agent.Random.Next(locations.Length); } 
                while(locations[rand] == LocationType.OutlawCamp || locations[rand] == LocationType.Empty || locations[rand] == LocationType.Mountain || locations[rand] == current);

            return locations[rand];
        }
    }

    public class EncounterOutlaw : State<Sheriff>
    {
        private Outlaw _outlaw;

        public EncounterOutlaw(Outlaw outlaw)
        {
            _outlaw = outlaw;
        }

        public override void Enter(Sheriff agent)
        {
            if(!_outlaw.IsAlive || _outlaw.LocationType == LocationType.OutlawCamp)
                return;
                        
            if(Agent.Random.Next(0, 10) == 0)
            {
                Printer.Print(agent.Name, "Ugh!");
                agent.IsAlive = false;
                agent.StateMachine.ChangeState(new KilledByBandit());

                Game1.Instance.WorldMap.Get(agent.Position).AddCorpse(agent.ID);

                var undertakers = AgentManager.GetAgents(a => (a is Undertaker.Undertaker));
                if(undertakers.Count > 0)
                {
                    Agent undertaker = undertakers[Sheriff.Random.Next(undertakers.Count)];
                    Message.DispatchMessage(0, agent.ID, undertaker.ID, MessageType.GunFight);
                }

                return;
            }

            Printer.Print(agent.Name, "Bang bang!");
            Message.DispatchMessage(0, agent.ID, _outlaw.ID, MessageType.DearSirYouHaveBeenShotPleaseDie);
            agent.GoldAmount = _outlaw.TakeGold();
            agent.StateMachine.ChangeState(new TravellingSheriffState(new DropOffGoldInBank()));
        }

        public override void Execute(Sheriff agent)
        {
            
        }

        public override void Exit(Sheriff agent)
        {

        }

        public override bool OnMesssage(Sheriff agent, Telegram telegram)
        {
            return false;
        }
    }

    public class DropOffGoldInBank : State<Sheriff>
    {
        public override LocationType GetInitialLocation()
        {
            return LocationType.Bank;
        }

        public override void Enter(Sheriff agent)
        {
            agent.GoldAmount = 0;
            Printer.Print(agent.Name, "Returning gold");
        }

        public override void Execute(Sheriff agent)
        {
            agent.StateMachine.ChangeState(new TravellingSheriffState(new VisitSaloon()));
        }

        public override void Exit(Sheriff agent)
        {

        }

        public override bool OnMesssage(Sheriff agent, Telegram telegram)
        {
            return false;
        }
    }

    public class VisitSaloon : State<Sheriff>
    {
        public override LocationType GetInitialLocation()
        {
            return LocationType.Saloon;
        }

        public override void Enter(Sheriff agent)
        {
            Printer.Print(agent.Name, "Visit saloon");
        }

        public override void Execute(Sheriff agent)
        {
            agent.StateMachine.ChangeState(new TravellingSheriffState(new GoToOffice()));
        }

        public override void Exit(Sheriff agent)
        {

        }

        public override bool OnMesssage(Sheriff agent, Telegram telegram)
        {
            return false;
        }
    }

    public class KilledByBandit : State<Sheriff>
    {
        private bool _corpseCollected = false;

        public override void Enter(Sheriff agent)
        {
            Printer.Print(agent.Name, "Sheriff dead");
        }

        public override void Execute(Sheriff agent)
        {
            if(!_corpseCollected)
                return;

            agent.Position = Game1.Instance.WorldMap.NodeOf(LocationType.SherrifsOffice).Position;
            agent.StateMachine.ChangeState(new GoToOffice());
        }

        public override void Exit(Sheriff agent)
        {
            agent.IsAlive = true;
        }

        public override bool OnMesssage(Sheriff agent, Telegram telegram)
        {
            if(telegram.messageType == MessageType.CorpseCollected)
            {
                _corpseCollected = true;
                return true;
            }

            return false;
        }
    }
    
    public class SheriffGlobalState : State<Sheriff>
    {

        public override void Enter(Sheriff agent)
        {

        }

        public override void Execute(Sheriff agent)
        {

        }

        public override void Exit(Sheriff agent)
        {

        }

        public override bool OnMesssage(Sheriff agent, Telegram telegram)
        {
            return false;
        }
    }
}
