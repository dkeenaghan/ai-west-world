﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace dkeenaghan.westworld
{
    public class WorldMapNode : IDrawableObject
    {
        public Vector2 Position { get; set; }
        public LocationType LocationType { get; set; }
        public Color HighlightColor { get; set; }
        public bool IsHighlighted { get; set; }
        public int HighlightIndex { get; set; }

        private Texture2D _texture;
        private Texture2D _transpBackground;
        private WorldMap _map;
        /// <summary>
        /// The value of the entry in the list corrosponds to the ID of the agent to whom the corpse belongs
        /// </summary>
        private List<int> _corpseList = new List<int>();

        public WorldMapNode(int col, int row, LocationType location, WorldMap map)
        {
            _texture = Game1.Instance.Content.Load<Texture2D>(location.ToString());
            _transpBackground = new Texture2D(Game1.Instance.GraphicsDevice, 1, 1);
            _transpBackground.SetData(new Color[] { Color.White });
            IsHighlighted = false;
            Position = new Vector2(col, row);
            LocationType = location;
            _map = map;
        }

        public void AddCorpse(int agentID)
        {
            _corpseList.Add(agentID);
        }

        public void ClearCorpses()
        {
            _corpseList.Clear();
        }

        public List<int> GetCorpses()
        {
            return _corpseList;
        }

        public void Update(GameTime gameTime)
        {
            if(HighlightIndex == 0)
                HighlightColor = new Color(0.5f, 0.0f, 0.0f, 0.3f);
            if(HighlightIndex == 1)
                HighlightColor = new Color(0.0f, 0.5f, 0.0f, 0.3f);
            if(HighlightIndex == 2)
                HighlightColor = new Color(0.0f, 0.0f, 0.5f, 0.3f);
            if(HighlightIndex == 3)
                HighlightColor = new Color(0.5f, 0.5f, 0.0f, 0.3f);
            if(HighlightIndex == 4)
                HighlightColor = new Color(0.5f, 0.0f, 0.5f, 0.3f);
            if(HighlightIndex == 5)
                HighlightColor = new Color(0.0f, 0.5f, 0.5f, 0.3f);
            if(HighlightIndex == 6)
                HighlightColor = new Color(0.4f, 0.7f, 0.4f, 0.3f);
            if(HighlightIndex > 6)
                HighlightColor = new Color(0.9f, 0.6f, 0.3f, 0.3f);
        }

        public void Draw(GameTime gameTime)
        {
            Vector2 pos = new Vector2(Position.X * _texture.Width, Position.Y * _texture.Height);
            
            Game1.Instance.SpriteBatch.Draw(_texture, pos, Color.White);

            if(IsHighlighted && WorldMap.ShowTrails)
                Game1.Instance.SpriteBatch.Draw(_transpBackground, new Rectangle((int)pos.X, (int)pos.Y, _texture.Width, _texture.Height), HighlightColor);

            if(Game1.Instance.WorldMap.IsTwilight)
                Game1.Instance.SpriteBatch.Draw(_transpBackground, new Rectangle((int)pos.X, (int)pos.Y, _texture.Width, _texture.Height), new Color(0.0f, 0.0f, 0.0f, 0.25f));

            if(Game1.Instance.WorldMap.IsNightTime)
                Game1.Instance.SpriteBatch.Draw(_transpBackground, new Rectangle((int)pos.X, (int)pos.Y, _texture.Width, _texture.Height), new Color(0.0f, 0.0f, 0.0f, 0.5f));
        }
    }
}
