﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dkeenaghan.westworld
{
    // Here is the list of locations that agents can visit
    public enum LocationType
    {
        Shack,
        GoldMine,
        Bank,
        Saloon,
        OutlawCamp,
        SherrifsOffice,
        Undertakers,
        Cemetery,
        Empty,
        Mountain
    }
}
