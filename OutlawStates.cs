﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    public class TravellingOutlawState : State<Outlaw>
    {
        Vector2 _destination;
        Stack<WorldMapNode> _route;
        State<Outlaw> _nextState;

        public TravellingOutlawState(State<Outlaw> nextState)
        {
            _nextState = nextState;
            _destination = Game1.Instance.WorldMap.PositionOf(nextState.GetInitialLocation());
        }

        public override void Enter(Outlaw agent)
        {
            _route = Game1.Instance.WorldMap.GetRoute(agent.Position, _destination);
        }

        public override void Execute(Outlaw agent)
        {
            if(_route.Count == 0)
            {
                agent.StateMachine.ChangeState(_nextState);
                return;
            }

            WorldMapNode current = _route.Pop();
            agent.Position = current.Position;
            current.HighlightIndex = agent.ID;
            current.IsHighlighted = true;
        }

        public override void Exit(Outlaw agent)
        {
            Game1.Instance.WorldMap.UnhighlightAll(agent.ID);
        }

        public override bool OnMesssage(Outlaw agent, Telegram telegram)
        {
            return false;
        }

        protected virtual WorldMapNode NextLocation()
        {
            return null;
        }
    }

    public class VisitOutlawCamp : State<Outlaw>
    {
        public override LocationType GetInitialLocation()
        {
            return LocationType.OutlawCamp;
        }

        public override void Enter(Outlaw agent)
        {
            Printer.Print(agent.Name, "Visitin' tha camp");
        }

        public override void Execute(Outlaw agent)
        {
            int random = Agent.Random.Next(9);

            Printer.Print(agent.Name, "Lurkin...");

            if(random < 1)
                agent.StateMachine.ChangeState(new TravellingOutlawState(new RobBank()));
            else if(random < 4)
                agent.StateMachine.ChangeState(new TravellingOutlawState(new VisitCemetery()));
        }

        public override void Exit(Outlaw agent)
        {
            Printer.Print(agent.Name, "Time to mosey on off");
        }

        public override bool OnMesssage(Outlaw agent, Telegram telegram)
        {
            return false;
        }
    }

    public class VisitCemetery : State<Outlaw>
    {
        public override LocationType GetInitialLocation()
        {
            return LocationType.Cemetery;
        }

        public override void Enter(Outlaw agent)
        {
            Printer.Print(agent.Name, "Might be some gold in one of them dead people boxes");
        }

        public override void Execute(Outlaw agent)
        {
            int random = Agent.Random.Next(20);

            Printer.Print(agent.Name, "Did ya hear that?");

            if(random < 1)
                agent.StateMachine.ChangeState(new TravellingOutlawState(new RobBank()));
            else if(random < 4)
                agent.StateMachine.ChangeState(new TravellingOutlawState(new VisitOutlawCamp()));
        }

        public override void Exit(Outlaw agent)
        {
            Printer.Print(agent.Name, "I'm a getting the hell outta here!");
        }

        public override bool OnMesssage(Outlaw agent, Telegram telegram)
        {
            return false;
        }
    }

    public class RobBank : State<Outlaw>
    {
        public override LocationType GetInitialLocation()
        {
            return LocationType.Bank;
        }

        public override void Enter(Outlaw agent)
        {
            Printer.Print(agent.Name, "Time to get me some gold!");
        }

        public override void Execute(Outlaw agent)
        {
            int newGold = Outlaw.Random.Next(1, 11);
            agent.GoldAmount += newGold;

            Printer.Print(agent.Name, String.Format("Yee Haw! I got {0} more gold!", newGold));

            agent.StateMachine.ChangeState(new TravellingOutlawState(new VisitOutlawCamp()));
        }

        public override void Exit(Outlaw agent)
        {
            Printer.Print(agent.Name, "Now it's time to high tail it outta here");
        }

        public override bool OnMesssage(Outlaw agent, Telegram telegram)
        {
            return false;
        }
    }

    public class ShotDead : State<Outlaw>
    {
        private bool _corpseCollected = false;

        public override void Enter(Outlaw agent)
        {
            Printer.Print(agent.Name, "*DEAD*");
        }

        public override void Execute(Outlaw agent)
        {
            // wait until corpse has been collected before respawning
            if(!_corpseCollected)
                return;

            int newGold = Outlaw.Random.Next(1, 11);
            agent.GoldAmount += newGold;

            Printer.Print(agent.Name, "Gruuuuuuugh blaaaaeugh");

            agent.Position = Game1.Instance.WorldMap.NodeOf(LocationType.OutlawCamp).Position;
            agent.StateMachine.ChangeState(new VisitOutlawCamp());
        }

        public override void Exit(Outlaw agent)
        {
            agent.IsAlive = true;
        }

        public override bool OnMesssage(Outlaw agent, Telegram telegram)
        {
            if(telegram.messageType == MessageType.CorpseCollected)
            {
                _corpseCollected = true;
                return true;
            }

            return false;
        }
    }

    public class OutlawGlobalState : State<Outlaw>
    {
        public override void Enter(Outlaw agent)
        {
            
        }

        public override void Execute(Outlaw agent)
        {
            
        }

        public override void Exit(Outlaw agent)
        {
            
        }

        public override bool OnMesssage(Outlaw agent, Telegram telegram)
        {
            if(telegram.messageType == MessageType.DearSirYouHaveBeenShotPleaseDie)
            {
                agent.IsAlive = false;
                agent.StateMachine.ChangeState(new ShotDead());

                Game1.Instance.WorldMap.Get(agent.Position).AddCorpse(agent.ID);

                var undertakers = AgentManager.GetAgents(a => (a is Undertaker.Undertaker));
                if(undertakers.Count > 0)
                {
                    Agent undertaker = undertakers[Outlaw.Random.Next(undertakers.Count)];
                    Message.DispatchMessage(0, agent.ID, undertaker.ID, MessageType.GunFight);
                }

                return true;
            }

            return false;
        }
    }
}
