﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    /// <summary>
    /// The base type for all signals, any class that extends this class can be detected by an appropriate sensor.
    /// </summary>
    public abstract class Signal
    {
        public Vector2 Position { get; set; }
        public SenseType Type { get; protected set; }
        public IEmitter Emitter { get; set; }
    }

    public class SightSignal : Signal
    {
        public SightSignal(Vector2 position, IEmitter emitter)
        {
            Type = SenseType.Sight;

            Position = position;
            Emitter = emitter;
        }
    }

    public enum SenseType
    {
        Sight,
        Hearing,
        Smell
    }
}
