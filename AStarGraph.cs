﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    public abstract class AStarGraph
    {
        protected IAStarNode[,] _nodes;

        public AStarGraph(int width, int height)
        {
            _nodes = new IAStarNode[height, width];
        }
        
        /// <summary>
        /// Sets the properties of the nodes used in withing the A* algorithm to their default values
        /// This should be called after performing a search using this node
        /// </summary>
        public void ResetAll()
        {
            foreach(var item in _nodes)
            {
                item.F = 0;
                item.G = 0;
                item.H = 0;
                item.ParentNode = null;
            }
        }

        public void Set(Vector2 position, IAStarNode node)
        {
            Set((int)position.X, (int)position.Y, node);
        }

        public void Set(int col, int row, IAStarNode node)
        {
            _nodes[col, row] = node;
        }
        
        public IAStarNode Get(Vector2 position)
        {
            return Get((int)position.X, (int)position.Y);
        }

        public IAStarNode Get(int col, int row)
        {
            if(row < 0 || col < 0 || row >= _nodes.GetLength(1) || col >= _nodes.GetLength(0))
                return null;

            return _nodes[col, row];
        }

        public Vector2 GetPositionOf(IAStarNode node)
        {
            for(int row = 0; row < _nodes.GetLength(0); row++)
            {
                for(int col = 0; col < _nodes.GetLength(1); col++)
                {
                    if(_nodes[col, row] == node)
                        return new Vector2(col, row);
                }
            }

            return new Vector2(-1);
        }
    }
}
