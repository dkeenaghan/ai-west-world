﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    public interface ISensor
    {
        /// <summary>
        /// The location of the sensor
        /// </summary>
        Vector2 Position { get; set; }

        /// <summary>
        /// Used to check if the sensor is capable of detecting a particular type of signal
        /// </summary>
        /// <param name="signal">The signal to check against</param>
        /// <returns>true if it can detect the given signal</returns>
        bool DetectsSense(Signal signal);

        /// <summary>
        /// Gets the maximum distance the sensor can detect a signal from
        /// </summary>
        /// <param name="signal">The signal to check against</param>
        /// <returns>Maximum sense range</returns>
        float DetectRange(Signal signal);

        /// <summary>
        /// Returns the distance from the sensor to a given signal
        /// </summary>
        /// <param name="signal">The signal to check against</param>
        /// <returns>Distance to the signal</returns>
        float DistanceTo(Signal signal);

        /// <summary>
        /// The method that will be called when the sensor detects a signal
        /// </summary>
        /// <param name="e">Information pertaining to the particular sensing event</param>
        void OnSense(SenseEventArgs e);
    }
}
