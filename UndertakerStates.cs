﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld.Undertaker
{
    public class TravellingUndertakerState : State<Undertaker>
    {
        Vector2 _destination;
        Stack<WorldMapNode> _route;
        State<Undertaker> _nextState;

        public TravellingUndertakerState(WorldMapNode destination, State<Undertaker> nextState)
        {
            _nextState = nextState;
            _destination = destination.Position;
        }

        public override void Enter(Undertaker agent)
        {
            _route = Game1.Instance.WorldMap.GetRoute(agent.Position, _destination);
        }

        public override void Execute(Undertaker agent)
        {
            if(_route.Count == 0)
            {
                agent.StateMachine.ChangeState(_nextState);
                return;
            }

            WorldMapNode current = _route.Pop();
            agent.Position = current.Position;
            current.HighlightIndex = agent.ID;
            current.IsHighlighted = true;
        }

        public override void Exit(Undertaker agent)
        {
            Game1.Instance.WorldMap.UnhighlightAll(agent.ID);
        }

        public override bool OnMesssage(Undertaker agent, Telegram telegram)
        {
            return false;
        }

        protected virtual WorldMapNode NextLocation()
        {
            return null;
        }
    }

    public class WaitingForDeaths : State<Undertaker>
    {
        public override void Enter(Undertaker agent)
        {
            
        }

        public override void Execute(Undertaker agent)
        {
            Printer.Print(agent.Name, "Waiting for a gunfight");

            if(agent.CorpseLocations.Count > 0)
                agent.StateMachine.ChangeState(new TravellingUndertakerState(Game1.Instance.WorldMap.Get(agent.CorpseLocations.Dequeue()), new CollectingCorpses()));            
        }

        public override void Exit(Undertaker agent)
        {
            Printer.Print(agent.Name, "There's bodies to collect");
        }

        public override bool OnMesssage(Undertaker agent, Telegram telegram)
        {
            return false;
        }
    }

    public class CollectingCorpses : State<Undertaker>
    {        
        public CollectingCorpses()
        {

        }

        public override void Enter(Undertaker agent)
        {
            Printer.Print(agent.Name, "Yey corpses");
        }

        public override void Execute(Undertaker agent)
        {
            agent.StateMachine.ChangeState(new TravellingUndertakerState(Game1.Instance.WorldMap.NodeOf(LocationType.Cemetery), new RemovingToCemetary()));
        }

        public override void Exit(Undertaker agent)
        {
            var corpses = Game1.Instance.WorldMap.Get(agent.Position).GetCorpses();

            foreach (var corpseID in corpses)
            {
                Message.DispatchMessage(0, agent.ID, corpseID, MessageType.CorpseCollected);
            }

            Game1.Instance.WorldMap.Get(agent.Position).ClearCorpses();
        }

        public override bool OnMesssage(Undertaker agent, Telegram telegram)
        {
            return false;
        }
    }

    public class RemovingToCemetary : State<Undertaker>
    {
        public override void Enter(Undertaker agent)
        {
            Printer.Print(agent.Name, "cemetary time");
        }

        public override void Execute(Undertaker agent)
        {
            agent.StateMachine.ChangeState(new TravellingUndertakerState(Game1.Instance.WorldMap.NodeOf(LocationType.Undertakers), new WaitingForDeaths()));
        }

        public override void Exit(Undertaker agent)
        {

        }

        public override bool OnMesssage(Undertaker agent, Telegram telegram)
        {
            return false;
        }
    }

    public class UndertakerGlobalState : State<Undertaker>
    {
        public override void Enter(Undertaker agent)
        {

        }

        public override void Execute(Undertaker agent)
        {

        }

        public override void Exit(Undertaker agent)
        {

        }

        public override bool OnMesssage(Undertaker agent, Telegram telegram)
        {
            if(telegram.messageType == MessageType.GunFight)
            {
                agent.CorpseLocations.Enqueue(AgentManager.GetAgent(telegram.Sender).Position);
                return true;
            }

            return false;
        }
    }
}
