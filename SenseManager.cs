﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace dkeenaghan.westworld
{
    public class SenseManager
    {
        private List<ISensor> _sensors = new List<ISensor>();
        private List<Signal> _signals = new List<Signal>();

        private List<Tuple<ISensor, SenseEventArgs>> _pending = new List<Tuple<ISensor, SenseEventArgs>>();
        private List<Signal> _sensorSignals = new List<Signal>();

        public SenseManager()
        {
            
        }

        public void RegisterSensor(ISensor sensor)
        {
            _sensors.Add(sensor);
        }

        public void AddSignal(Signal signal)
        {
            _signals.Add(signal);
        }

        public void Update(GameTime gameTime)
        {
            float distance;
            
            for(int j = 0; j < _sensors.Count; j++)            
            {
                _sensorSignals.Clear();

                for(int i = 0; i < _signals.Count; i++)
                {
                    // if the sensor is detecting itself or it can't detect this signal then move on
                    if(_sensors[j] == _signals[i].Emitter || !_sensors[j].DetectsSense(_signals[i]))
                        continue;

                    distance = _sensors[j].DistanceTo(_signals[i]);

                    // if the signal is too far away to detect then move on
                    if(_sensors[j].DetectRange(_signals[i]) < distance)
                        continue;

                    _sensorSignals.Add(_signals[i]);                    
                }
                                
                if(_sensorSignals.Count > 0)
                    _pending.Add(new Tuple<ISensor, SenseEventArgs>(_sensors[j], new SenseEventArgs(_sensorSignals)));
            }
            
            _pending.ForEach(senseEvent => senseEvent.Item1.OnSense(senseEvent.Item2));
            _pending.Clear();
        }
    }
    
    public class SenseEventArgs : EventArgs
    {
        public List<Signal> Signals { get; set; }

        public SenseEventArgs(List<Signal> signals)
        {
            Signals = signals;
        }
    }
}
